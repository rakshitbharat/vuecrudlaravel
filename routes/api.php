<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'client'], function () {
    Route::get('/', 'ClientController@index');
    Route::post('add', 'ClientController@add');
    Route::get('edit/{id}', 'ClientController@edit');
    Route::post('update/{id}', 'ClientController@update');
    Route::delete('delete/{id}', 'ClientController@delete');
});