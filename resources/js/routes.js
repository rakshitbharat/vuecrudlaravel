import AllClient from './AllClient.vue';
import AddClient from './AddClient.vue';
import EditClient from './EditClient.vue';
import Partcode from './Partcode.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllClient
    },
    {
        name: 'add',
        path: '/add',
        component: AddClient
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditClient
    },
    {
        name: 'partcode',
        path: '/partcode',
        component: Partcode
    },
];