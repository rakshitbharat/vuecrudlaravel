<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    // all clients
    public function index()
    {
        $client = Client::all()->toArray();
        return array_reverse($client);
    }

    // add client
    public function add(Request $request)
    {
        $client = new Client([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ]);
        $client->save();

        return response()->json('The client successfully added');
    }

    // edit client
    public function edit($id)
    {
        $client = Client::find($id);
        return response()->json($client);
    }

    // update client
    public function update($id, Request $request)
    {
        $client = Client::find($id);
        $client->update($request->all());

        return response()->json('The client successfully updated');
    }

    // delete client
    public function delete($id)
    {
        $client = Client::find($id);
        $client->delete();

        return response()->json('The client successfully deleted');
    }
}
